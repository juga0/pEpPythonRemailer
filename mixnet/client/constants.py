"""Constants."""
import os

ANGLE_ADDR = "<{}>"
NAME_ADDR = "{} {}"

N1_FP = "44CF7B7252148BE970F53FF8BF378725BCA73C60"
N1_NAME_ADDR = "n1 <root@n1.pep.example>"
N2_FP = "EEA6C082CA652EE1F6297F569CFD11A518CD28F8"
N2_NAME_ADDR = "n2 <root@n2.pep.example>"
N3_FP = "5CF81E8377654F1BD1D5A02B4854BBDC9957C746"
N3_NAME_ADDR = "n3 <root@n3.pep.example>"

NUMBER_REMAILERS = 3

# Constants to compose Emails compatible with Mixmaster
REMAILER_KEY_SUBJECT = "remailer-key"
REMAILER_BODY = """::
Anon-To: {to}

##
Subject: {subject}

{body}"""
REMAILER_ENCRYPTED_PREFIX = """::
Encrypted: PGP

{}
"""

REMAILER_ADDRESS = "remailer@dizum.com"
REMAILER_NAME_ADDR = "Nomen Nescio <remailer@dizum.com>"
REMAILER_FP = "69AAA31095EA74254AF6BEBCDE7947C81698D34C"
REMAILER_KEYS = os.path.join(
    os.path.dirname(__file__), "..", "data", "pgp-all.asc"
)
