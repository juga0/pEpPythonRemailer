"""Mixnet client library."""
import logging
import random
import time

from mixnet import common
from mixnet.client import constants

logger = logging.getLogger(__name__)


def get_nodes():
    """Return identity list.

    It should call:
    PEP_STATUS get_registered_nodes(
        PEP_SESSION session,
        identity_list **nodes
    );
    For now just return identities.
    """
    n1 = common.set_identity(constants.N1_NAME_ADDR, constants.N1_FP)
    n2 = common.set_identity(constants.N2_NAME_ADDR, constants.N2_FP)
    n3 = common.set_identity(constants.N3_NAME_ADDR, constants.N3_FP)
    return [n3, n2, n1]


def select_route(nodes, number=constants.NUMBER_REMAILERS):
    """Return several nodes randomly. Dummy route algorithm.."""
    nodes = random.sample(nodes, number)
    logger.info("Route to send message: %s.", [str(node) for node in nodes])
    return nodes


def onionize(me, msg, route):
    for node in route:
        msg = common.create_outgoing_msg(me, node, "", str(msg))
        msg = msg.encrypt()
        logger.info(
            "Message from %s encrypted to %s.", str(msg.from_), str(msg.to[0])
        )
    # logger.debug("Onionized msg: %s", msg)
    return msg


def process(
    from_,
    to,
    from_fpr="",
    subject="",
    body="",
    host="localhost",
    port=25,
    send=True,
):
    me = common.set_my_identity(from_, from_fpr)
    recipient = common.set_identity(to)
    logger.debug(recipient)
    msg = common.create_outgoing_msg(me, recipient, subject, body)
    enc_msg = msg.encrypt()
    logger.info("Message encrypted for %s.", str(recipient))
    nodes = get_nodes()
    # route = select_route(nodes)
    route = nodes
    onion_msg = onionize(me, enc_msg, route)
    delay = common.dummy_delay()
    time.sleep(delay)
    if send:
        common.send_email(str(onion_msg), host, port)
    # and return msg to do something else with it, as to use it in tests
    return onion_msg
