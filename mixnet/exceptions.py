"""Exceptions."""


class EfarException(Exception):
    pass


class PepNoKeyImported(EfarException):
    def __init__(self):
        super().__init__("No key imported.")


class PepKeyNotFound(EfarException):
    def __init__(self):
        super().__init__("No key found.")


class EmailNotEncrypted(EfarException):
    def __init__(self):
        super().__init__("Email not encrypted.")


class EmailNotDecrypted(EfarException):
    def __init__(self):
        super().__init__("Email not decrypted.")


class EmailNotTrusted(EfarException):
    def __init__(self):
        super().__init__("Email not trusted.")


class NoEmailAddress(EfarException):
    def __init__(self):
        super().__init__("No Email address.")
