"""Mixnet remailer library."""
import logging
import os
import time

from mixnet import common, exceptions

logger = logging.getLogger(__name__)


def decrypt_email(email_str):
    """Decrypt an incoming Email and forward it to the next hop.

    There's not need to check that the recipient of the Email is this
    remailer, if it can be decrypted, it'll be forwarded.

    The decrypted Email, the one to forward, won't probably have this
    remailer as sender and does not need to.

    :param str email_str: incoming Email
    :returns email.message.Message: the Email to forward
    """
    import pEp

    msg = common.create_incoming_msg(email_str)
    dec_msg, key_list, rating, _r = msg.decrypt()
    logger.debug(
        "decrypt_email: key_list %s, rating %s, _r %s", key_list, rating, _r
    )
    # XXX: r is always 0?
    if not key_list:
        raise exceptions.EmailNotDecrypted
    # XXX: Add defaults to peppythonadapter
    # XXX: is it yellow >= 0?
    if pEp.color(rating) < 0:
        raise exceptions.EmailNotTrusted
    logger.info("Next hop: %s", dec_msg.longmsg.split("\r\n")[1])
    return dec_msg.longmsg


def process_email(
    email_str, mynameaddress="", myfpr="", host="localhost", port=25, send=True
):
    logger.debug("$HOME=%s", os.environ["HOME"])
    gnupg_home = os.environ.get("GNUPGHOME", None)
    if gnupg_home:
        logger.debug("$GNUPGHOME=%s", os.environ["GNUPGHOME"])
    try:
        _ = common.set_my_identity(mynameaddress, myfpr)  # noqa:F841
    except (exceptions.NoEmailAddress, exceptions.PepKeyNotFound) as e:
        logger.error(e)
    # register_node(me)
    dec_email_str = decrypt_email(email_str)
    delay = common.dummy_delay()
    time.sleep(delay)
    if send:
        common.send_email(dec_email_str, host, port)
    # and return the msg in case something else is done like with tests
    return dec_email_str


def register_node(identity):
    """Register this node.

    It should call:
     ``PEP_STATUS register_node(PEP_SESSION session, pEp_identity *ident);
    It does nothing yet.

    """
    pass
