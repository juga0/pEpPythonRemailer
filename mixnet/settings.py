"""Create settings singleton.

That can be updated at run time.

"""
from . import defaults


class Settings:
    def __init__(self) -> None:
        """Set attributes from the default constants."""
        for default in dir(defaults):
            if default.isupper():
                setattr(self, default, getattr(defaults, default))


# Create the singleton settings object
settings = Settings()
