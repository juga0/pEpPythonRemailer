"""Constants."""


# XXX: Create these first constants as part of the adapter
OUTGOING_MSG = 1
INCOMING_MSG = 2

BASIC_EMAIL_RE = r"[^@]+@[^@]+\.[^@]+"

# Mixing delays
# Warning: Arbitrarely choosen as proof of concept.
# Maximum is low to do not have to wait much the tests.
MIN_DELAY_SECS = 0
MAX_DELAY_SECS = 60 * 60  # 1h

# For running tests
TEST_NET = False

# Custom logging config
LOG_PATH = "log/mixnet.log"
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {"format": "{levelname} {message}", "style": "{"},
        "color": {
            "()": "colorlog.ColoredFormatter",
            "format": "%(log_color)s%(levelname)s:%(name)s:%(message)s",
            "log_colors": {
                "DEBUG": "cyan",
                "INFO": "green",
                "WARNING": "yellow",
                "ERROR": "red",
                "CRITICAL": "red,bg_white",
            },
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "color"},
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "mixnet.log",
            "level": "DEBUG",
        },
    },
    "loggers": {
        "mixnet": {
            "level": "INFO",
            "handlers": ["console", "file"],
            "propagate": True,
        }
    },
}
