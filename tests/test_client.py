"""client unit tests."""
from mixnet.client import client

from .constants import ALICE_FP, ALICE_NAME_ADDR, BOB_NAME_ADDR, SUBJECT_A_B


def test_process_email(
    email_from_alice_to_bob, import_keys, import_public_keys
):
    """Process Email received by Bob from Alice."""
    email = client.process(
        ALICE_NAME_ADDR,
        BOB_NAME_ADDR,
        ALICE_FP,
        SUBJECT_A_B,
        email_from_alice_to_bob,
        send=False,
    )
    assert email.to[0].address == "root@n1.pep.example"
    assert email.shortmsg == "p≡p"
    assert email.enc_format == 3
    # XXX: Decrypt the email to check is correctly encrypted
