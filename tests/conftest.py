"""pytest configuration for the unit tests."""
import pytest

from mixnet import common, settings

settings.TEST_NET = True


@pytest.fixture()
def datadir(request):
    """Get, read, open test files from the tests "data" directory."""

    class D:
        def __init__(self, basepath):
            self.basepath = basepath

        def open(self, name, mode="r"):
            return self.basepath.join(name).open(mode)

        def join(self, name):
            return self.basepath.join(name).strpath

        def read(self, name):
            with self.open(name, "r") as f:
                return f.read()

        def readlines(self, name):
            with self.open(name, "r") as f:
                return f.readlines()

    return D(request.fspath.dirpath("data"))


@pytest.fixture(scope="function")
def tmpdir(tmpdir_factory, request):
    """Create a tmp dir for the tests."""
    base = str(hash(request.node.nodeid))[:3]
    bn = tmpdir_factory.mktemp(base)
    return bn


@pytest.fixture()
def alice_sec_key_path(datadir):
    key_data = datadir.basepath.join("alice@openpgp.example.sec.asc")
    return key_data


@pytest.fixture()
def bob_sec_key_path(datadir):
    key_data = datadir.basepath.join("bob@openpgp.example.sec.asc")
    print(type(key_data))
    return key_data


@pytest.fixture()
def carol_sec_key_path(datadir):
    key_data = datadir.basepath.join("carol@openpgp.example.sec.asc")
    return key_data


@pytest.fixture()
def email_signed_encrypted_from_bob_to_carol(datadir):
    email = datadir.read("signed_encrypted_from_bob_to_carol.eml")
    return email


@pytest.fixture()
def email_signed_encrypted_from_alice_to_bob(datadir):
    email = datadir.read("signed_encrypted_from_alice_to_bob.eml")
    return email


@pytest.fixture()
def email_from_alice_to_bob(datadir):
    email = datadir.read("plain_from_alice_to_bob.eml")
    return email


@pytest.fixture()
def email_from_bob_to_carol(datadir):
    email = datadir.read("plain_from_bob_to_carol.eml")
    return email


@pytest.fixture(scope="function")
def set_env(tmpdir):
    return common.set_env(str(tmpdir))


@pytest.fixture(scope="function")
def import_keys(
    set_env, bob_sec_key_path, alice_sec_key_path, carol_sec_key_path, datadir
):
    common.import_keys(
        [bob_sec_key_path, alice_sec_key_path, carol_sec_key_path]
    )


@pytest.fixture()
def email_encrypted_from_alice_to_n3(datadir):
    email = datadir.read("encrypted_from_Alice Lovelace_to_n3.eml")
    return email


@pytest.fixture(scope="function")
def import_public_keys(set_env, datadir):
    # XXX: Add alice's key
    common.import_keys(
        [
            datadir.basepath.join("bob@openpgp.example.asc"),
            datadir.basepath.join("root@n1.pep.example.asc"),
            datadir.basepath.join("root@n2.pep.example.asc"),
            datadir.basepath.join("root@n3.pep.example.asc"),
        ]
    )
