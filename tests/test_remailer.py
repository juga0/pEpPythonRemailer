"""remailer unit tests."""
import email

import pytest

from mixnet import common, exceptions
from mixnet.client import constants
from mixnet.remailer import remailer

from .constants import (
    ALICE_FP,
    ALICE_NAME_ADDR,
    BOB_FP,
    BOB_NAME_ADDR,
    BODY,
    CAROL_FP,
    CAROL_NAME_ADDR,
    SUBJECT_A_B,
    SUBJECT_B_C,
)


def test_encrypt_decrypt(import_keys):
    alice = common.set_my_identity(ALICE_NAME_ADDR, ALICE_FP)
    bob = common.set_identity(BOB_NAME_ADDR, BOB_FP)
    msg_a_b = common.create_outgoing_msg(alice, bob, SUBJECT_A_B, BODY)
    enc_msg_a_b = msg_a_b.encrypt()
    assert enc_msg_a_b.enc_format == 3

    dec_msg_a_c, _key_list, _rating, _r = enc_msg_a_b.decrypt()
    assert dec_msg_a_c.longmsg.replace("\r", "") == msg_a_b.longmsg
    # 'from Alice to Bob' != 'p≡p'
    assert not dec_msg_a_c.shortmsg == msg_a_b.shortmsg
    assert dec_msg_a_c.to[0].address == msg_a_b.to[0].address
    assert dec_msg_a_c.from_.address == msg_a_b.from_.address


def test_encrypt_twice_decrypt(import_keys):
    """Encrypt twice, then decrypt twice.

    Alice encrypts first a message for Bob, then encrypts the encrypted
    message for Carol.
    Carol receives de message, decrypt, and sends the body of the decrypted
    message to Bob.
    Bob receives, decrypt it and see the message that Alice sent.
    The communication happens then in this direction:

    Alice -> Carol -> Bob
    """
    alice = common.set_my_identity(ALICE_NAME_ADDR, ALICE_FP)
    bob = common.set_identity(BOB_NAME_ADDR, BOB_FP)
    # msg_a_b from Alice to Bob
    msg_a_b = common.create_outgoing_msg(alice, bob, SUBJECT_A_B, BODY)
    enc_msg_a_b = msg_a_b.encrypt()
    assert enc_msg_a_b.enc_format == 3

    carol = common.set_identity(CAROL_NAME_ADDR, CAROL_FP)
    # msg_a_c from Alice to Carol including msg_a_b to Bob
    msg_a_c = common.create_outgoing_msg(
        alice, carol, SUBJECT_B_C, str(enc_msg_a_b)
    )
    # and encrypts it
    enc_msg_a_c = msg_a_c.encrypt()
    assert enc_msg_a_c.enc_format == 3

    ##
    # msg_a_c arrives to Carol, who decrypts it
    dec_msg_a_c, _key_list, _rating, _r = enc_msg_a_c.decrypt()
    # Carol sees the msg is for Bob and sends it to bob
    # NOTE: msg_a_b needs to be reconstructed to parse the MIME
    msg_c_b = common.create_incoming_msg(dec_msg_a_c.longmsg)

    # msg_a_b arrives to Bob, who decrypts it
    dec_msg_a_b, _key_list, _rating, _r = msg_c_b.decrypt()
    assert dec_msg_a_b.longmsg.replace("\r", "") == BODY
    assert not dec_msg_a_b.shortmsg == msg_a_b.shortmsg
    assert dec_msg_a_b.to[0].address == msg_a_b.to[0].address
    assert dec_msg_a_b.from_.address == msg_a_b.from_.address


def test_decrypt_email_succeed(
    import_keys,
    email_signed_encrypted_from_alice_to_bob,
    email_signed_encrypted_from_bob_to_carol,
):
    email_to_send = remailer.decrypt_email(
        email_signed_encrypted_from_alice_to_bob
    )
    email_lines = email_to_send.split("\r\n")
    assert email_lines[0] == "From: Bob Babagge <bob@openpgp.example>"
    assert email_lines[1] == "To: Carol Hopper <carol@openpgp.example>"
    assert (
        str(email_to_send).replace("\r", "")
        == email_signed_encrypted_from_bob_to_carol
    )


def test_decrypt_email_not_encrypted(import_keys, email_from_bob_to_carol):
    with pytest.raises(exceptions.EmailNotDecrypted) as e:
        assert remailer.decrypt_email(email_from_bob_to_carol)
    assert str(e.value) == "Email not decrypted."


# Since pEp-2.1.0rc2, it'd fail with
# Failed: DID NOT RAISE <class 'mixnet.exceptions.EmailNotDecrypted'>
@pytest.mark.skip(
    reason="When run with tox, it does not create .pEp/keys.db, "
    "but it's taking the keys somewhere"
)
def test_decrypt_email_no_keys(
    set_env, email_signed_encrypted_from_alice_to_bob
):
    with pytest.raises(exceptions.EmailNotDecrypted) as e:
        assert remailer.decrypt_email(email_signed_encrypted_from_alice_to_bob)
    assert str(e.value) == "Email not decrypted."


# Since pEp-2.1.0rc2, it'd fail with
# Failed: DID NOT RAISE <class 'mixnet.exceptions.EmailNotDecrypted'>
@pytest.mark.skip(
    reason="When run with tox, it does not create .pEp/keys.db, "
    "but it's taking the keys somewhere"
)
def test_set_identity_decrypt(
    set_env,
    bob_sec_key_path,
    email_from_bob_to_carol,
    email_signed_encrypted_from_alice_to_bob,
):
    """Test setting my identity with/without existing key and decrypt."""
    # XXX: Some of these tests should be moved to peppythonadapter

    # Test that my identity can't be set without an Email address.
    with pytest.raises(exceptions.NoEmailAddress) as e:
        common.set_my_identity("foo")
    assert str(e.value) == "No Email address."

    # Test that my identity can be set with a name addr.
    common.set_my_identity(BOB_NAME_ADDR)
    # XXX: Get bob key
    # and test that bob can decrypt an email encrypted with that key

    # Test that bob can't decrypt an Email encrypted for other key.
    with pytest.raises(exceptions.EmailNotDecrypted):
        email_to_send = remailer.decrypt_email(
            email_signed_encrypted_from_alice_to_bob
        )

    # Test that identity can't be set with a fingerprint if the key is not
    # imported.
    with pytest.raises(exceptions.PepKeyNotFound) as e:
        common.set_my_identity(BOB_NAME_ADDR, BOB_FP)
    assert str(e.value) == "No key found."

    # Test that identity can be set with a fingerprint if the key is imported.
    common.import_keys(bob_sec_key_path)
    common.set_my_identity(BOB_NAME_ADDR, BOB_FP)
    email_to_send = remailer.decrypt_email(
        email_signed_encrypted_from_alice_to_bob
    )
    assert (
        email_to_send.split("\r\n")[0]
        == "From: Bob Babagge <bob@openpgp.example>"
    )


def test_process_email(email_encrypted_from_alice_to_n3, import_keys):
    """Process Email received by n3 from Alice."""
    mail_str = remailer.process_email(
        email_encrypted_from_alice_to_n3,
        constants.N3_NAME_ADDR,
        constants.N3_FP,
        send=False,
    )
    mail = email.message_from_string(mail_str)
    assert mail["From"] == ALICE_NAME_ADDR
    assert mail["To"] == BOB_NAME_ADDR
    assert mail["Subject"] == "=?utf-8?Q?p=E2=89=A1p?="
