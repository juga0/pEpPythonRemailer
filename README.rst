pEp MixMailer prototype in Python
===================================

This project is a prototype of the pEp MixMailer project described in
[Design]_ .
It is composed by two subpackages:

1. ``client``

It encrypts an Email message several times as described in and send it to the
first remailer.

2. ``remailer``

It decrypts an OpenPGP encrypted Email and sends it to the recipient(s)
`in the decrypted Email.
It is intented to run as Postfix pipe.

Documentation
-------------

See the ``docs`` directory.

Issues
------

Please, report any bug or feature request at the `issue tracker`_.

Contributing
------------

See ``CONTRIBUTING.md``

License
-------

GPLv3

Authors
-------

juga at riseup dot net


[Design] https://mixmailer_docs.codeberg.page/Design/Proposal1.html.
.. _issue tracker: https://gitea.pep.foundation/pEp.foundation/mixmailer/issues
