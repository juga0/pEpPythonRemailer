#!/bin/bash

set -x

mkdir -p ./tmp/authority/.config
mkdir -p ./tmp/n1/.config
chown -R 1000:1000 ./tmp/authority ./tmp/n1
docker-compose up -d gnunet.n1.pep.example authority.pep.example
sleep 3
AUTHORITY_IP=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' authority.pep.example`
N1_IP=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' gnunet.n1.pep.example`

# Could not find how to open the ports in dind, so if it is running there,
# just show it
if [ ! -z `hostname|grep runner` ]
then
    # In case this is run in docker dind (docker in docker, as in Gitlab CI),
    # the ports can be opened. This part tests that.
    docker network inspect peppythonmixnet_default
    /sbin/ifconfig
    # Ports 7770 and 7776 should be open!!
    apk add --no-cache nmap
    nmap -sT -p 7776 localhost
    nmap -sT -p 7776 -N $AUTHORITY_IP
        curl http://$N1_IP:7776/identity/
        curl http://$AUTHORITY_IP:7770/identity/
        curl http://172.17.0.4:7776/identity
        curl http://172.17.0.4:7770/identity
else
    # Run the tests
    echo "Test how would a node register itself in GNS via its GNUnet node and register to an Authority."
    echo "In gnunet.n1.pep.example node, create an A record.\n"
    curl -X POST -H "Content-Type: application/json" -d '{"name": "root"}' http://$N1_IP:7776/identity/
    echo "\n"
    curl http://$N1_IP:7776/identity/
    echo "\n"

    echo "Check the identity and store it to use it in authority"
    GNUNET1=$(docker exec gnunet.n1.pep.example gnunet-identity -d | grep root | awk '{print $3}')
    echo $GNUNET1

    curl -X PUT -H "Content-Type: application/json" -d '{"subsystem": "namestore"}' http://$N1_IP:7776/identity/subsystem/root/
    echo "\n"
    curl  http://$N1_IP:7776/identity/subsystem/namestore/
    echo "\n"
    curl -H "Content-Type: application/json" -d '{"record_name": "n1", "data": {"value": "172.19.0.2", "record_type": "A"}}' http://$N1_IP:7776/namestore/root/
    echo "\n"
    curl http://$N1_IP:7776/gns/n1.root
    echo "\n"

    echo "In gnunet.n1.pep.example node, attempt to create a CERT record and fail.\n"
    curl -H "Content-Type: application/json" -d '{"record_name": "n1cert", "data": {"value": "AAAA", "record_type": "CERT"}}' http://$N1_IP:7776/namestore/root/
    echo "\n"
    # {"error": "Data invalid"}

    echo "In gnunet.n1.pep.example node, create a TXT record.\n"
    curl -H "Content-Type: application/json" -d '{"record_name": "n1", "data": {"value": "email=root@n1.pep.example;layer=1;opengpg=AAAA", "record_type": "TXT"}}' http://$N1_IP:7776/namestore/root/
    echo "\n"
    # curl -H "Content-Type: application/json" -d '{"record_name": "n1txt", "data": {"value": "email=root@n1.pep.example;layer=1;opengpg=AAAA", "record_type": "TXT"}}' http://$N1_IP:7776/namestore/root/
    curl http://$N1_IP:7776/gns/n1.root
    echo "\n"
    # It has 2 n1.root records, one the A type, the other the TXT type
    curl http://$N1_IP:7776/namestore/root/
    echo "\n"


    echo "In authority.pep.example node, create an A record.\n"
    curl -X POST -H "Content-Type: application/json" -d '{"name": "root"}' http://$AUTHORITY_IP:7776/identity/
    echo "\n"
    curl http://$AUTHORITY_IP:7776/identity/
    echo "\n"
    curl -X PUT -H "Content-Type: application/json" -d '{"subsystem": "namestore"}' http://$AUTHORITY_IP:7776/identity/subsystem/root/
    curl  http://$AUTHORITY_IP:7776/identity/subsystem/namestore/
    echo "\n"
    curl -H "Content-Type: application/json" -d '{"record_name": "authority", "data": {"value": "172.19.0.2", "record_type": "A"}}' http://$AUTHORITY_IP:7776/namestore/root/
    echo "\n"
    curl http://$AUTHORITY_IP:7776/gns/authority.root
    echo "\n"


    echo "In authority.pep.example node, delegate gnunet.n1.pep.example records.\n"
    curl -H "Content-Type: application/json" -d '{"record_name": "n1", "data": {"value": "$GNUNET1", "record_type": "PKEY"}}' http://$AUTHORITY_IP:7776/namestore/root/
    echo "\n"
    curl http://$AUTHORITY_IP:7776/gns/n1.n1.root
    echo "\n"
    # it takes some time to be available, how much?, but shows only A one.
    curl http://$AUTHORITY_IP:7776/namestore/root/
    echo "\n"
    # same
fi
docker-compose stop
