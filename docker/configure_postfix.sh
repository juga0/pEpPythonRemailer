#!/bin/bash

set -exuo pipefail

echo 'Configuring postfix'
# In the case that the containers are not run with docker-compose, each
# container needs to know its domain and the other containers domain.
# Configure a local domain
# XXX: replace lines instead of adding
# echo '127.0.0.1' $DOMAIN >> /etc/hosts
# echo '::1 '$DOMAIN >> /etc/hosts

# echo '# Make the host solve these domains:'>> /etc/hosts
# echo '172.17.0.1 n1.pep.example n2.pep.example n3.pep.example openpgp.example'>> /etc/hosts

# Configure postfix master.cf
# To run postfix in a docker container
postconf -F '*/*/chroot = n'
# To run postfix in a different port than 25
postconf -F 'smtp/inet/service='$PORT

if [ $TRANSPORT == 1 ]; then
    # The transport will run by default as `postfix` user, therefore pass the user
    # to run the transport.
    # In the case that Postfix runs in a different port than 25, pass it to the
    # transport too.
    postconf -M remailer/unix='remailer unix - n n - - pipe user='$USER' argv=/usr/local/bin/remailer -n -p '$MYFP' -k  '$KEY #'-t localhost:'$PORT

    # Add transport, for the remailer case
    echo $DOMAIN'        remailer:' > /etc/postfix/transport
    # echo '# To be able to deliver email to remote hosts in a different port' >> /etc/postfix/transport
    # echo 'n1.pep.example  smtp:n1.pep.example:2501' >> /etc/postfix/transport
    # echo 'n2.pep.example  smtp:n2.pep.example:2502' >> /etc/postfix/transport
    # echo 'n3.pep.example  smtp:n3.pep.example:2503' >> /etc/postfix/transport
    # echo 'openpgp.example  smtp:openpgp.example:2504' >> /etc/postfix/transport
    postconf -e transport_maps=hash:/etc/postfix/transport
    postmap /etc/postfix/transport

fi

# Configure postfix main.cf
postconf -e myhostname=$DOMAIN
# Don't add 0.0.0.0 in a production server, it will become an open relay!!
# It's added here so that it can send email to other domains without authentication
postconf -e mynetworks="0.0.0.0/0 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128"
postconf -e smtp_host_lookup=dns,native

#Add virtual
echo '@'$DOMAIN'         root@'$DOMAIN > /etc/postfix/virtual
postconf -e virtual_alias_maps=hash:/etc/postfix/virtual
# Create postfix dbs
postmap /etc/postfix/virtual

exec "$@"
